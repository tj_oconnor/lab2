<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab2</title>
        
        <link href="css/default.css" rel="stylesheet">
	</head>
    <body>                
        <h1 class="title">Recipe Database</h1>
		<div id="container">
			<form action="insert.php" method="post">
				<fieldset>
					<legend>Recipe Card</legend>
					<hr />
					<div id="titleDiv">
						<input class="title" type="text" placeholder="Title" name="title">
					</div>
					<div id="cardBodyDiv">
						<div id="ingDiv">
							<input class="ingredient" type="text" placeholder="Ingredient" name="ingredient0">
							<hr />
							<input class="ingredient" type="text" placeholder="Ingredient" name="ingredient1">
							<hr />
							<input class="ingredient" type="text" placeholder="Ingredient" name="ingredient2">
							<hr />
							<input class="ingredient" type="text" placeholder="Ingredient" name="ingredient3">
							<hr />
						</div>
					
						<div id="instructDiv">
							<textarea id="instructions" name="instructions" class="instructions" placeholder="Instructions"></textarea>
						</div>
					</div>
					<button type="submit" class="submitBTN" name="submit">Submit</button>
					
				</fieldset>
			</form>

		</div>
        
    </body>
</html>

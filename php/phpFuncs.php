<?php

function getTitle(){
	echo htmlentities($_POST['title']);
}

function getIngred(){
	$index = 0;
	$more = true;
	while($more){
		if (htmlentities((!isset($_POST['ingredient'.$index])))) {
			$more = false;
		}else{
			echo htmlentities($_POST['ingredient'.$index]);
			echo '<hr />';
			$index++;
		}
	}
}

function getInst(){
	echo htmlentities($_POST['instructions']);
}
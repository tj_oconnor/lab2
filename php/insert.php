<?php
include "phpFuncs.php";
if ((!isset($_POST['submit']))) {
    header("Location: error.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab2</title>
        <link href="css/default.css" rel="stylesheet">
    </head>
    <body>                
        <div class="container">
        	<h1 class="title">Recipe Database</h1>        
	        <h2>Thank You For Your Submission <em></em></h2>	        
	        <div id="container">
				<fieldset>
					<legend>Recipe Card</legend>
					<hr />
					<div id="titleDiv">
						<?php getTitle(); ?>
					</div>
					<div id="cardBodyDiv">
						<div id="ingDiv">
							<?php getIngred(); ?>
						</div>
						<div id="instructDiv">
							<?php getInst(); ?>
						</div>
					</div>
					
				</fieldset>
		</div>
    </body>
</html>
